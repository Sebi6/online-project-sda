package com.onlinestore.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Address {

    @Id
    @GeneratedValue
    private Long id;

    private String county;
    private String city;
    private String street;
    private String zipcode;
    private int number;
    @ManyToOne()
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
//        if (user!=null){
//            user.getUserAddress().add(this);
//        }
//      uitate in user dto la modificare nu ii dam userDto.getAddress ii dam direct lista de addresses
        //legatura bidirectionala address-user
        //this adresa curenta pe care vrem sa o
//        adaugam la utilizator ,partea aceasta adauga adresa curenta
    }

    public void removeUserLink(){
        this.user=null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
