package com.onlinestore.entities;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "user_orders")
public class Order {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;

    private double total;

    @OneToMany(cascade = CascadeType.ALL)//cand salvam orderul salvam si product order copy
    private List<ProductOrderCopy> productOrderCopies;

    @CreationTimestamp//va salva in date data din momentul in care a fost adauat in DB
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<ProductOrderCopy> getProductOrderCopies() {
        return productOrderCopies;
    }

    public void setProductOrderCopies(List<ProductOrderCopy> productOrderCopies) {
        this.productOrderCopies = productOrderCopies;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
