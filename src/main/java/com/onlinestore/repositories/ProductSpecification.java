package com.onlinestore.repositories;

import com.onlinestore.entities.Category;
import com.onlinestore.entities.Product;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class ProductSpecification implements Specification<Product> {

    public static Specification<Product> withNameLike(String s){
        return (root, query, criteriaBuilder) ->criteriaBuilder
                .like(root.get("name"),"%" + s + "%");
// root=obiectul de tipul product
    }

    public static Specification<Product> withMaxPrice(Double price){
        return (root, query, criteriaBuilder) ->criteriaBuilder
                .lt(root.get("price"),price);
    }

    public static Specification<Product> withMinPrice(Double price){
        return (root, query, criteriaBuilder) ->criteriaBuilder
                .gt(root.get("price"),price);
    }

    public static Specification<Product> withCategory(Category category){
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("category"),category);
    }

    public static Specification<Product> withCategoryId(Long id){
        return (root, query, criteriaBuilder) -> {
            Join<Product,Category> categoryJoin=root.join("category");
            return criteriaBuilder.equal(categoryJoin.get("id"),id);
        };
    }


    @Override
    public Specification<Product> and(Specification<Product> other) {
        return Specification.super.and(other);
    }

    @Override
    public Specification<Product> or(Specification<Product> other) {
        return Specification.super.or(other);
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return null;
    }
}
