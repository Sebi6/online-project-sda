package com.onlinestore.controllers.dto;

import com.onlinestore.entities.Category;
import com.onlinestore.entities.Product;
import com.onlinestore.services.CategoryService;
import org.springframework.security.core.parameters.P;

public class ProductDto {
    private Long id;

    private String name;
    private String description;
    private String imageUrl;
    private double price;
    private long stock;
    private Long categoryId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getStock() {
        return stock;
    }

    public void setStock(long stock) {
        this.stock = stock;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public static Product toProduct(ProductDto productDto, CategoryService categoryService){
        Product product=new Product();
        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setImageURl(productDto.getImageUrl());
        product.setStock(productDto.getStock());
        product.setPrice(productDto.getPrice());
        Category category=categoryService.findById(productDto.getCategoryId());
        product.setCategory(category);
        return product;

    }
    public static ProductDto toProductDto(Product product){
        ProductDto productDto=new ProductDto();
        productDto.setName(product.getName());
        productDto.setDescription(product.getDescription());
        productDto.setPrice(product.getPrice());
        productDto.setStock(product.getStock());
        productDto.setImageUrl(product.getImageURl());
        productDto.setCategoryId(product.getCategory().getId());
        productDto.setId(product.getId());
        return productDto;
    }
}
