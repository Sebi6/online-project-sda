package com.onlinestore.controllers.dto;

import com.onlinestore.entities.Product;
import com.onlinestore.entities.ShoppingCart;

import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCartDto {
    private List<ProductDto> productDto;


    public List<ProductDto> getProductDto() {
        return productDto;
    }

    public void setProductDto(List<ProductDto> productDto) {
        this.productDto = productDto;
    }
    public static ShoppingCartDto toShoppingCartDto(ShoppingCart shoppingCart){
        ShoppingCartDto shoppingCartDto=new ShoppingCartDto();
        List<ProductDto> productDtoList=shoppingCart.getProductsInCart().stream()
                .map(productInCart->ProductDto.toProductDto(productInCart))
                .collect(Collectors.toList());
        shoppingCartDto.setProductDto(productDtoList);
        return shoppingCartDto;
    }
}
