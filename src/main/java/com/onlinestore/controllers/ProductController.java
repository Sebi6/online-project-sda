package com.onlinestore.controllers;

import com.onlinestore.controllers.dto.ProductDto;
import com.onlinestore.entities.Product;
import com.onlinestore.services.CategoryService;
import com.onlinestore.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.GeneratedValue;
import java.util.Map;

@RestController
@RequestMapping(path = "/products")
public class ProductController {
    private ProductService productService;
    private CategoryService categoryService;

    @Autowired
    public ProductController(ProductService productService,CategoryService categoryService){
        this.productService=productService;
        this.categoryService=categoryService;

    }

    @PostMapping
    public Product save(@RequestBody ProductDto productDto){
        Product product=ProductDto.toProduct(productDto,categoryService);
        return productService.save(product);
    }

    @GetMapping
    public Page<Product> search(@RequestParam Map<String ,String> params){
        return productService.search(params);
    }
}
