package com.onlinestore.controllers;

import com.onlinestore.entities.Category;
import com.onlinestore.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/categories")
public class CategoryController {
    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping
    public Category create(@RequestBody Category category){
        return categoryService.save(category);
    }
    @GetMapping
    public List<Category> findAll(){
        return categoryService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Category findById(@PathVariable Long id){
        return categoryService.findById(id);
    }

    @DeleteMapping(path = "/{id}")
    public HttpStatus delete(@PathVariable Long id){
        categoryService.delete(id);
        return HttpStatus.OK;
    }

    @PutMapping(path = "/{id}")
    public Category update(@PathVariable Long id,@RequestBody Category category){
        category.setId(id);
        return categoryService.update(category);
    }

}
