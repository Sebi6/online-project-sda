package com.onlinestore.services;

import com.onlinestore.entities.Order;

public interface OrderService {

    Order save();

    Order getLoggedUserOrders();
}
