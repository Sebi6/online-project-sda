package com.onlinestore.services;

import com.onlinestore.entities.ShoppingCart;
import com.onlinestore.entities.User;

import java.util.List;

public interface ShoppingCartService {
    ShoppingCart addProductToCart(Long productId);
    ShoppingCart removeProductFromCart(Long productId);
    ShoppingCart getShoppingCartOfLoggedUser();
    ShoppingCart resetShoppingCart();


}
