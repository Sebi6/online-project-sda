package com.onlinestore.services;

import com.onlinestore.entities.User;
import com.onlinestore.exceptions.ResourceAlreadyPresentInDatabase;
import com.onlinestore.exceptions.ResourceMissingInDatabase;
import com.onlinestore.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImplementation implements UserService, UserDetailsService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder encoder;

    @Autowired
    public UserServiceImplementation(UserRepository userRepository){
        this.userRepository=userRepository;
        this.encoder=new BCryptPasswordEncoder();
    }

    @Override
    public User save(User user) {
        User userInDatabase=userRepository.findByEmail(user.getEmail());
        if (userInDatabase!=null){
            throw new ResourceAlreadyPresentInDatabase(String.format("User with %s email 0already exists.",user.getEmail()));
        }
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
//        cum sa salvat trimitem si email
    }

    @Override
    public User findByEmail(String email) {
        User user=userRepository.findByEmail(email);
        if (user==null){
            throw new ResourceMissingInDatabase(String.format("User with email %s not found",email));
        }
        return user;
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(String email) {
        userRepository.delete(findByEmail(email));
    }

    @Override
    public User getLoggedUser() {
        UserDetails userDetails=(UserDetails)
                SecurityContextHolder//obiect spring care ajuta la identificarea utilizatorului curent logat
                        .getContext()
                        .getAuthentication()
                        .getPrincipal();
        return userRepository.findByEmail(userDetails.getUsername());
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user=findByEmail(email);
        List<GrantedAuthority> grantedAuthorityList=new ArrayList<>();
        grantedAuthorityList.add(new SimpleGrantedAuthority(user.getRole().getName()));
        UserDetails userDetails=new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),grantedAuthorityList );
        return userDetails;
    }

















}
