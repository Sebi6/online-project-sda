package com.onlinestore.services;

import com.onlinestore.entities.Product;
import com.onlinestore.entities.ShoppingCart;
import com.onlinestore.entities.User;
import com.onlinestore.repositories.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShoppingCartServiceImplementation implements ShoppingCartService{

    private ShoppingCartRepository shoppingCartRepository;
    private ProductService productService;
    private UserService userService;

    @Autowired
    public ShoppingCartServiceImplementation(ShoppingCartRepository shoppingCartRepository,
                                             ProductService productService,
                                             UserService userService) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productService = productService;
        this.userService = userService;
    }

    @Override
    public ShoppingCart addProductToCart(Long productId) {
   //     User user= userService.getLoggedUser();//gasim utilizatorul loat
   //    ShoppingCart shoppingCart= user.getShoppingCart();//gasim cosul de cumparaturi asociat userului logat
        ShoppingCart shoppingCart=getShoppingCartOfLoggedUser();
        List<Product> productsAlreadyInCart=shoppingCart.getProductsInCart();//luam lista existenta de produse
        Product product=productService.findById(productId);//identificam noul produs de adaugat in cos
        productsAlreadyInCart.add(product);//la lista veche adugam noul produs
        shoppingCart.setProductsInCart(productsAlreadyInCart);//updatam lista de produse din shopping cart
        return shoppingCartRepository.save(shoppingCart);//salvam noua lista de produse din shopping cart
    }

    @Override
    public ShoppingCart removeProductFromCart(Long productId) {
        ShoppingCart shoppingCart=getShoppingCartOfLoggedUser();
        List<Product> productsAlreadyInCart=shoppingCart.getProductsInCart();
        Product product=productService.findById(productId);
        productsAlreadyInCart.remove(product);
        shoppingCart.setProductsInCart(productsAlreadyInCart);
        return shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public ShoppingCart getShoppingCartOfLoggedUser(){
//        ShoppingCart shoppingCart=userService.getLoggedUser().getShoppingCart();
//        if (shoppingCart!=null){
//            return shoppingCart;
//        }else {
//            shoppingCart=new ShoppingCart();
//            shoppingCart.setUser(userService.getLoggedUser());
//            User savedLoggedUser =userService.getLoggedUser();
//            savedLoggedUser.setShoppingCart(shoppingCart);
//            userService.update(savedLoggedUser);
//        }
//        return shoppingCart;

        User user= userService.getLoggedUser();
        if (user.getShoppingCart()!=null){
            return user.getShoppingCart();
        } else {
            ShoppingCart shoppingCart=new ShoppingCart();
            shoppingCart.setUser(user);
            user.setShoppingCart(shoppingCart);
            userService.update(user);
        }
        return user.getShoppingCart();
    }

    @Override
    public ShoppingCart resetShoppingCart() {
        ShoppingCart shoppingCart=getShoppingCartOfLoggedUser();
        shoppingCart.setProductsInCart(new ArrayList<>());
        return shoppingCartRepository.save(shoppingCart);
    }
}
